# _gte

Détermine une condition permettant une comparaison supérieure ou égale. L'opérateur **_gte** extrait les POI dont la propriété visée est supérieure ou égale à la valeur fournie.

```graphql
{
    poi(
        filters: [
            { <propriété>: {_gte: <valeur>} }
        ]
    )
    {
        results {
            <propriété_extraite>
        }
    }
}
```

### Exemples ###

```graphql
{
  poi(
    filters: [
      {
        allowedPersons: {
          _gte: 30
        }
      }
    ]
  )
  {
    results{
      dc_identifier
    }
  }
}
```
Cette requête extraira tous les identifiants des **POI** où le nombre de personnes autorisées (**allowedPersons**) est supérieur ou égal à 30. Le résultat sera de ce type :
```json
{
  "data": {
    "poi": {
      "results": [
        {
          "dc_identifier": [
            "TFO308495934640"
          ]
        },
        {
          "dc_identifier": [
            "TFO130946193200"
          ]
        }
      ]
    }
  }
}
```