# _lt

Détermine une condition permettant une comparaison strictement inférieure. L'opérateur **_lt** extrait les POI dont la propriété visée est strictement inférieure à la valeur fournie.

```graphql
{
    poi(
        filters: [
            { <propriété>: {_lt: <valeur>} }
        ]
    )
    {
        results {
            <propriété_extraite>
        }
    }
}
```

### Exemples ###

```graphql
{
  poi(
    filters: [
      {
        allowedPersons: {
          _lt: 30
        }
      }
    ]
  )
  {
    results{
      dc_identifier
    }
  }
}
```
Cette requête extraira tous les identifiants des **POI** où le nombre de personnes autorisées (**allowedPersons**) est inférieur à 15. Le résultat sera de ce type :
```graphql
{
  "data": {
    "poi": {
      "results": [
        {
          "dc_identifier": [
            "TFO073163031006"
          ]
        },
        {
          "dc_identifier": [
            "TFO096220245274"
          ]
        }
      ]
    }
  }
}
```