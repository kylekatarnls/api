<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\BlazegraphResolver;

use Datatourisme\Api\Resolver\SparqlResolver\QueryBuilder\QueryBuilder;
use Datatourisme\Api\Resolver\SparqlResolver\QueryBuilder\QueryBuilderFactory;
use Datatourisme\Api\Resolver\SparqlResolver\Sparql\Triplet;

class BlazegraphQueryBuilderFactory extends QueryBuilderFactory
{
    /**
     * @return QueryBuilder
     */
    public function createQueryBuilder()
    {
        $queryBuilder = parent::createQueryBuilder();
        $queryBuilder->addWhere(
            new Triplet(
                '<http://www.bigdata.com/queryHints#Query>',
                '<http://www.bigdata.com/queryHints#optimizer>',
                '"None"'
            )
        );

        return $queryBuilder;
    }
}
