<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver;

use Youshido\GraphQL\Execution\ResolveInfo;

interface ResolverInterface
{
    /**
     * Resolve an execution context.
     *
     * @param string      $entryPoint
     * @param ResolveInfo $info
     *
     * @return
     */
    public function resolve(string $entryPoint, $arguments, ResolveInfo $info);
}
