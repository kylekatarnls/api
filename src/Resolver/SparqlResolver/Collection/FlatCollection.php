<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Collection;

class FlatCollection extends AbstractCollection
{
    /**
     * @param $sparql
     *
     * @return $this
     */
    public function add($sparql)
    {
        if ($sparql instanceof FlatCollection) {
            foreach ($sparql as $item) {
                $this->add($item);
            }
        } else {
            return parent::add($sparql);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return implode(".\n", $this->getArrayCopy());
    }
}
