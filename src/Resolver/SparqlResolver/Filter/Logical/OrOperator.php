<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Filter\Logical;

use Datatourisme\Api\Resolver\SparqlResolver\Collection\UnionCollection;
use Datatourisme\Api\Resolver\SparqlResolver\Filter\LogicalFilterInterface;

class OrOperator implements LogicalFilterInterface
{
    public function getName()
    {
        return '_or';
    }

    public function generate($sets)
    {
        return new UnionCollection($sets);
    }
}
