<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql\Functions;

class Values
{
    private $_var;
    private $_values;

    public function __construct($var, $values)
    {
        $this->_var = $var;
        $this->_values = $values;
    }

    public function __toString()
    {
        return sprintf('VALUES %s { %s }', $this->_var, implode(' ', $this->_values));
    }
}
