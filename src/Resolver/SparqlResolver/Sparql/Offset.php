<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql;

class Offset
{
    private $_offset;

    public function __construct($offset)
    {
        $this->_offset = $offset;
    }

    public function __toString()
    {
        return 'OFFSET '.$this->_offset;
    }
}
