<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql;

class Select
{
    private $_select;

    public function __construct($select)
    {
        $this->_select = $select;
    }

    public function __toString()
    {
        return 'SELECT '.$this->_select;
    }
}
