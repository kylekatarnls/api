<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Schema\Field\Common;

use Datatourisme\Api\Schema\Field\AbstractField;
use Datatourisme\Api\Schema\Type\InputObject\FilterType;

class FilterField_RdfType extends AbstractField
{
    public function getName()
    {
        return 'rdf_type';
    }

    public function getUri()
    {
        return 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type';
    }

    public function getType()
    {
        return new FilterType();
    }
}
