<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Schema\Type\InputObject;

use Youshido\GraphQL\Type\InputObject\AbstractInputObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;

class GeoBoundingType extends AbstractInputObjectType
{
    public function build($config)
    {
        $config
            ->addField('sw_lng', [
                'type' => new StringType(),
                'resolve' => function ($value) {
                    return $value;
                },
            ])
            ->addField('sw_lat', [
                'type' => new StringType(),
                'resolve' => function ($value) {
                    return $value;
                },
            ])
            ->addField('ne_lng', [
                'type' => new StringType(),
                'resolve' => function ($value) {
                    return $value;
                },
            ])
            ->addField('ne_lat', [
                'type' => new StringType(),
                'resolve' => function ($value) {
                    return $value;
                },
            ]);
    }

    public function getDescription()
    {
        return 'Réalise une requête géographique sur la base d\'une zone';
    }
}
