<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Schema\Type\Scalar;

class BooleanType extends \Youshido\GraphQL\Type\Scalar\BooleanType
{
    public function isValidValue($value)
    {
        return is_null($value) || $this->isBool($value);
    }

    public function isBool($var)
    {
        if (is_bool($var)) {
            return true;
        }
        switch (strtolower($var)) {
            case '1':
            case 'true':
            case 'on':
            case 'yes':
            case 'y':
                return true;
            default:
                return false;
        }
    }
}
